<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderItemRepository")
 */
class OrderItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order", inversedBy="order_items")
     * @ORM\JoinColumn(nullable=true)
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Item", inversedBy="order_items")
     * @ORM\JoinColumn(nullable=true)
     */
    private $item;

    public function getId()
    {
        return $this->id;
    }
}
