<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MealItemRepository")
 */
class MealItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $discount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Item", inversedBy="meal_items")
     * @ORM\JoinColumn(nullable=true)
     */
    private $item;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Meal", inversedBy="meal_items")
     * @ORM\JoinColumn(nullable=true)
     */
    private $meal;

    public function getId()
    {
        return $this->id;
    }

    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    public function setDiscount(int $discount): self
    {
        $this->discount = $discount;

        return $this;
    }
}
