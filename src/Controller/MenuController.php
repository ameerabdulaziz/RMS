<?php

namespace App\Controller;

use App\Entity\Menu;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller
{
    /**
     * @Route("/menus", name="menus")
     */
    public function index()
    {
        $menus = $this->getDoctrine()->getRepository(Menu::class)->findAll();

        return $this->render('menu/index.html.twig', [
            'menus' => $menus,
        ]);
    }
}
